collaborators: Kirsten Baker and Zack Considine
core classes: 
- Party: game world class that is instantiated each time a party starts
- PartyModel: model for the party that determines guests, items, etc.
- Controller: javafx controller that deals with button clicks, etc.
- Guest: class for guests that might come to the party
- Accessory: class for items that you can display at the party to attract guests (they have either been provided at the start of the game or given to you by guests)
 

Unfortunately, our sim is not totally complete. The console text explains what happens when button clicks occur, but we had a last minute crisis with javafx displaying accross multiple scenes and we were not able to get our graphical display working properly. Please take pity on us!
