package sample;

/**
 * This class creates and modifies party guests within the Party
 * gameworld.
 * @author Kirsten Baker and Zack Considine
 * @since 2016-05-25
 * @param name This is the guest's name
 * @param img This is the guest's sprite image
 * @param status The guest requires that the user's party has
 *               at least this much status to arrive
 * @param gift This is the gift that the guest brings to a party
 */

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Guest extends Group{

    private ImageView imageView;
    private String name;
    private Integer status;
    private Boolean atParty;
    private Accessory gift;
    private Boolean giftGiven;

    public Guest(String name, String img, Integer status, Accessory gift) {
        this.name = name;
        this.status = status;
        this.gift = gift;
        this.giftGiven = false;
        Image image = new Image(getClass().getResourceAsStream(img));
        this.imageView = new javafx.scene.image.ImageView();
        this.imageView.setImage(image);
        this.getChildren().add(this.imageView);
        this.atParty = false;
    }

    public final String getName() {
        return this.name;
    }

    public final void setName(String newName) {
        this.name = newName;
    }

    public final Accessory getGift() {
        this.giftGiven = true;
        return this.gift;
    }
    public final Boolean getGiftGiven() {
        return this.giftGiven;
    }

    public final Integer getStatus() {
        return this.status;
    }

    public final Boolean getAtParty() {
        return this.atParty;
    }

    public final Point2D getPosition() {
        Point2D position = new Point2D(this.getLayoutX(), this.getLayoutY());
        return position;
    }

    public final void setPosition(double x, double y) {
        this.setLayoutX(x);
        this.setLayoutY(y);
    }

    public final void remove() {
        this.atParty = false;
    }

    public final void insert() {
        this.atParty = true;
    }

}