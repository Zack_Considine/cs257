package sample;

/**
 * This is the gameworld class for Dinner Party. It loads the UI,
 * determines which guests will show up upon presentation of certain
 * items, and allows the user to change views.
 * @author Kirsten Baker and Zack Considine
 * @since 2016-05-25
 */
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.*;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Party extends Application {

    final private double SCENE_WIDTH = 800;
    final private double SCENE_HEIGHT = 600;

    private ArrayList<Guest> guestList, allPossibleGuests, justAdded;
    private ArrayList<Accessory> accessoryList, onDisplay;
    private ArrayList<Text> addedGifts;
    private Integer points, partyStatus;

    public static StackPane homeScreen, inventoryScreen, guestScreen;

    private Stage thestage;
    private Group root;
    private Scene homeScene, inventoryScene, guestScene;
    protected Controller controller;
    public static PartyModel partyModel;


    /**
     * This method initializes the (empty) party main view when the application
     * starts running, and it creates the complete lists of guests and accessories.
     * (Current two scene example code from javafxtutorials - scene2 will be visual
     * guest and item lists)
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) throws IOException{

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });

        thestage=primaryStage;
        controller = new Controller();

        homeScreen = FXMLLoader.load(Party.class.getResource("homeScreen.fxml"));
        inventoryScreen = FXMLLoader.load(Party.class.getResource("inventoryScreen.fxml"));
        guestScreen = FXMLLoader.load(Party.class.getResource("guestScreen.fxml"));


        addedGifts = new ArrayList<Text>();
        partyStatus = 4;
        points = 0;

        homeScene = new Scene(homeScreen);
        inventoryScene = new Scene(inventoryScreen);
        guestScene = new Scene(guestScreen);

        controller.startParty();
        partyModel = controller.returnPartyModelInstance();
        controller.partyModel = partyModel;

        primaryStage.setTitle("The Dinner Party");
        primaryStage.setScene(homeScene);
        primaryStage.show();

    }

    public static void displayCurGuests() {
        for (int i = 0; i < partyModel.guestList.size(); i++) {
            partyModel.guestList.get(i).setPosition(i * 50 + 50, 150);
            guestScreen.getChildren().add(partyModel.guestList.get(i));
        }
    }

    public static void displayAddedGuests() {
        for (int i = 0; i < partyModel.justAdded.size(); i++) {
            partyModel.justAdded.get(i).setPosition(i * 50 + 50, 150);
            homeScreen.getChildren().add(partyModel.justAdded.get(i));
        }
    }

    public static void displayInv() {
        for (int i = 0; i < partyModel.accessoryList.size(); i++) {
            partyModel.accessoryList.get(i).setPosition(i * 50 + 50, 150);
            inventoryScreen.getChildren().add(partyModel.accessoryList.get(i));
        }
    }

    public static void clearAddedGuests() {
        for (int i = 0; i < partyModel.justAdded.size(); i++) {
            homeScreen.getChildren().remove(partyModel.justAdded.get(i));
        }
    }

    public static void clearCurGuests() {
        for (int i = 0; i < partyModel.guestList.size(); i++) {
            guestScreen.getChildren().remove(partyModel.guestList.get(i));
        }
    }

    public static void clearInv() {
        for (int i = 0; i < partyModel.accessoryList.size(); i++) {
            inventoryScreen.getChildren().remove(partyModel.accessoryList.get(i));
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
