package sample;

/**
 * This class creates and modifies the items that the user owns,
 * displays, and receives.
 * @author Kirsten Baker and Zack Considine
 * @since 2016-05-25
 * @param name This is the name of the accessory
 * @param img This is the accessory's sprite image
 * @param points This is the number of points added to the
 *               user's status when the item is displayed
 */
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Accessory extends Group {

    private String name;
    private Integer points;
    private Boolean atParty;
    private Boolean inventoried;
    private ImageView imageView;

    public Accessory(String name, String img, Integer points) {
        this.name = name;
        this.points = points;
        Image image = new Image(getClass().getResourceAsStream(img));
        this.imageView = new ImageView();
        this.imageView.setImage(image);
        this.getChildren().add(this.imageView);
        this.inventoried = false;
        this.atParty = false;
    }

    public final void putInInventory() {
        this.inventoried = true;
    }

    public final String getName() {
        return this.name;
    }

    public final void setName(String newName) {
        this.name = newName;
    }

    public final Integer getPoints() {
        return this.points;
    }

    public final Boolean getAtParty() {
        return this.atParty;
    }

    public final Point2D getPosition() {
        Point2D position = new Point2D(this.getLayoutX(), this.getLayoutY());
        return position;
    }

    public final void setPosition(double x, double y) {
        this.setLayoutX(x);
        this.setLayoutY(y);
    }

    public final void remove() {
        this.atParty = false;
    }

    public final void insert() {
        this.atParty = true;
    }
}

