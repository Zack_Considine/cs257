/*
PartyModel.java
Created by Kirsten Baker and Zack Considine
cs257

Party Model Class handles the arraylists and sorting of the guests
and accessories. It also holds the algorithms and methods for
determining who comes to the party next.
 */

package sample;

import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by baker on 6/6/16.
 */
public class PartyModel {

    //Create the lists
    protected ArrayList<Guest> guestList, allPossibleGuests, justAdded;
    protected ArrayList<Accessory> accessoryList, onDisplay;
    protected ArrayList<Text> addedGifts;
    protected Integer points, partyStatus;

    /**
     * This method constructs the
     * PartyModel class by initializing all
     * of the lists and creating the user's
     * starting party status and points.
     * This method calls createAllPossibleGuestsAndAccessories
     * which creates all of the objects that the program
     * will eventually use.
     */
    public PartyModel() {

        addedGifts = new ArrayList<Text>();
        this.partyStatus = 420;
        this.points = 0;

        this.justAdded = new ArrayList<Guest>();
        this.guestList = new ArrayList<Guest>();
        this.allPossibleGuests = new ArrayList<Guest>();
        this.accessoryList = new ArrayList<Accessory>();
        this.onDisplay = new ArrayList<Accessory>();

        createAllPossibleGuestsAndAccessories();
        this.onDisplay.add(accessoryList.get(0));
        this.onDisplay.add(accessoryList.get(1));

        //Update the status of the party and the position of
        //the accessories.
        updateAtParty();
    }

    public void createAllPossibleGuestsAndAccessories(){

        Accessory vimBook = new Accessory("Vim Textbook", "/res/vimBook.jpg", 13);
        Accessory baldEagle = new Accessory("Bald Eagle", "/res/baldEagle.png", 20);
        Accessory bouillabaisse = new Accessory("Bouillabaisse", "/res/bouillabaisse.jpg", 22);
        Accessory chipsAndGuac = new Accessory("Chips and Guac", "/res/chipsAndGuac.jpg", 17);
        Accessory bobDPoster = new Accessory("Bob Dylan Poster", "/res/bobDPoster.jpg", 14);
        Accessory coolVibes = new Accessory("Cool Vibes", "/res/coolVibes.jpg", 8);
        Accessory pina = new Accessory("Pina Colada", "/res/pina.jpg", 33);
        Accessory standUp = new Accessory("Stand Up Comedy", "/res/standComedy.jpg", 35);
        Accessory charitableDonation = new Accessory("A Charitable Donation", "/res/charitableDonation.jpg", 20);
        Accessory crepes = new Accessory("Crepes", "/res/crepes.png", 16);
        Accessory burglarAlarm = new Accessory("Burglar Alarm", "/res/burglarAlarm.png", 22);
        Accessory giftsForEveryone = new Accessory("Gifts for everyone!", "/res/giftsForEveryone.png", 28);
        Accessory chaiLatte = new Accessory("A chai latte", "/res/chaiLatte", 15);
        Accessory chewedUpShoe = new Accessory("A chewed up shoe", "/res/chewedUpShoe.png", 1);
        Accessory signedDookie = new Accessory("A signed copy of smash hit record Dookie", "/res/signedDookie.png", 19);
        Accessory reassuringWords = new Accessory("Reassuring words", "/res/reassuringWords.png", 34);
        Accessory dalmation = new Accessory("A dalmation", "/res/dalmation.png", 24);

        Guest johnM = new Guest("John Mulaney", "/res/johnMulaney.jpg", 150, standUp);
        Guest mrs_headphones = new Guest("Mrs. Headphones", "/res/mrs_headphones.jpg", 470, coolVibes);
        Guest dudeHawaiian = new Guest("Dude in a Hawaiian shirt", "/res/dudeHawaii.jpg", 485, pina);
        Guest jet = new Guest("Jet", "/res/jet.png", 350, vimBook);
        Guest obama = new Guest("Obama", "/res/obama.png", 5, baldEagle);
        Guest gordonRamsay = new Guest("Gordon Ramsay", "/res/gordonRamsay.jpg", 25, bouillabaisse);
        Guest markZuckerberg = new Guest("Mark Zuckerberg", "/res/markZuckerberg.png", 200, charitableDonation);
        Guest larryFromFrench = new Guest("Larry from French class", "/res/larryFromFrench.png", 380, crepes);
        Guest macaulayCaulkin = new Guest("Macaulay Caulkin", "/res/macaulayCaulkin.png", 250, burglarAlarm);
        Guest oprah = new Guest("Oprah", "res/oprah.png", 75, giftsForEveryone);
        Guest yourLocalBarista = new Guest("Your Local Barista", "/res/yourLocalBarista.png", 300, chaiLatte);
        Guest confusedDog = new Guest("A confused dog", "/res/confusedDog.png", 120, chewedUpShoe);
        Guest greenDay = new Guest("Green Day", "/res/greenDay", 80, signedDookie);
        Guest compsAdvisor = new Guest("Your comps advisor", "/res/compsAdvisor.png", 40, reassuringWords);
        Guest volunteerFirefighter = new Guest("A volunteer firefighter", "/res/volunteerFirefighter", 60, dalmation);

        this.allPossibleGuests.add(markZuckerberg);
        this.allPossibleGuests.add(larryFromFrench);
        this.allPossibleGuests.add(macaulayCaulkin);
        this.allPossibleGuests.add(oprah);
        this.allPossibleGuests.add(yourLocalBarista);
        this.allPossibleGuests.add(confusedDog);
        this.allPossibleGuests.add(greenDay);
        this.allPossibleGuests.add(compsAdvisor);
        this.allPossibleGuests.add(volunteerFirefighter);
        this.allPossibleGuests.add(johnM);
        this.allPossibleGuests.add(jet);
        this.allPossibleGuests.add(obama);
        this.allPossibleGuests.add(gordonRamsay);
        this.allPossibleGuests.add(mrs_headphones);
        this.allPossibleGuests.add(dudeHawaiian);

        this.accessoryList.add(chipsAndGuac);
        this.accessoryList.add(bobDPoster);

        updateAccessoryList();
        updateAtParty();
        String message = this.updatePartyStatus();
        System.out.println(message);
    }

    /**
     * This method updates the accessory list
     * which is your inventory. It is called when the Take gifts button
     * is clicked and it adds the accessories that are on your current
     * guests but not inventoried to your inventory.
     * It prints a result string.
     */
    public void updateAccessoryList() {
        for (int i = 0; i < this.guestList.size(); i++) {
            if (!this.accessoryList.contains(this.guestList.get(i).getGift())) {
                this.guestList.get(i).getGift().insert();
                this.accessoryList.add(this.guestList.get(i).getGift());
                System.out.println(this.guestList.get(i).getGift().getName() + " has been added to your inventory!");
            }
        }
    }

    /**
     * This method updates the guests at the party
     * by removing them from the list of possible guests
     * and adding them to the guest list.
     * This method also updates the Accessories in a similar fashion.
     * If the accessories in the inventory were added to the party
     * then it prints a result string for that case.
     */
    public void updateAtParty() {
        for (int i = 0; i < allPossibleGuests.size(); i++) {
            allPossibleGuests.get(i).remove();
        }
        for (int i = 0; i < guestList.size(); i++) {
            guestList.get(i).insert();
        }
        for (int i = 0; i < this.accessoryList.size(); i++) {
            if (this.accessoryList.get(i).getAtParty() == true) {
                if (!this.onDisplay.contains(this.accessoryList.get(i))) {
                    this.onDisplay.add(this.accessoryList.get(i));
                    System.out.println("You have put " + this.accessoryList.get(i).getName() + " on display!");
                }
            }
            else {
                if (this.onDisplay.contains(this.accessoryList.get(i))) {
                    this.onDisplay.remove(this.accessoryList.get(i));
                }
            }
        }

    }

    /**
     * This method looks for any possible new guests.
     * It calls the generate guests method. A number of
     * allowed guests at a time can be set. Currently set to 1.
     * This method also prints a result string based on who was
     * added to the party.
     */
    public void refresh(Integer numberOfGuests) throws IOException {
        this.justAdded.removeAll(this.justAdded);
        Party.clearAddedGuests();
        this.updateAtParty();
        String message = this.updatePartyStatus();
        System.out.println(message);
        for (int i = 0; i < numberOfGuests; i++) {
            this.generateGuest(this.partyStatus);
        }
        Party.displayAddedGuests();
        String instr = "";
        if (this.justAdded.size() == 0) {
            System.out.println("You have no new guests.");
        }
        else {
            if (justAdded.size() <= 2) {
                instr = instr.concat(justAdded.get(0).getName() + " ");
            }
            else {
                for (int i = 0; i < justAdded.size(); i++) {
                    instr = instr.concat(justAdded.get(i).getName() + ", ");
                }
            }
            System.out.println(instr + "has come to your party!");
        }
    }
    /**
     * This method updates the party status
     * by taking the initial party status and subtracting
     * all of the points of each accessory on display.
     * It also subtracts the total quantity of accessories
     * on display. The minimum status is 1.
     * This method also returns a result string which
     * tells the user the status of the party.
     */
    public String updatePartyStatus() {
        int minS = 420;
        for (int i = 0; i < this.onDisplay.size(); i++) {
            minS = minS - this.onDisplay.get(i).getPoints();
            if (minS <= 1) {
                minS = 1;
                break;
            }
        }
        this.partyStatus = minS;
        String accent;
        if (this.partyStatus > 400) {
            accent = "lowkey boring.";
        }
        else if (this.partyStatus > 300) {
            accent = "aight.";
        }
        else if (this.partyStatus > 200) {
            accent = "chilll!";
        }
        else if (this.partyStatus > 100) {
            accent = "off the hook!";
        }
        else {
            accent = "official.";
        }
        return ("Your party status is " + this.partyStatus.toString() + ". Your party is " + accent).toString();
    }

    /**
     * This method determines the new guests to be added to the party based on the user's
     * status (determined by the sum of the points from the current items on display). First,
     * the possible guests are determined by iterating through the sorted guest list and
     * finding the guests with high enough status to be interested in the party (high status
     * is indicative of less discerning guests- this will be explained momentarily). All of these
     * guests' statuses are added together, then for each guest a decimal range is generated based
     * on their fraction. For example, if guest A has status 25, B has 20, and C has 5, then
     * guest A has range 0.0-0.5, B has 0.5-0.9, and C has 0.9-1.0. Then a random number between 0
     * and 1 is generated (to a predetermined decimal value) and the range it is in determines
     * which guest is added to the party. This is repeated until all of the previous guests are
     * replaced (the guests already added in this cycle will not be available to add again).
     * @param curStatus This is the party's current status, determined by the items on display
     */
    public void generateGuest(int curStatus) {
        ArrayList<Guest> couldAdd = new ArrayList<Guest>();
        ArrayList<Guest> sortedGuests = this.sortGuests();

        int sumStatus = 0;
        ArrayList<Float> statusProbs = new ArrayList<Float>();
        for (int i = 0; i < sortedGuests.size(); i++) {
            if (sortedGuests.get(i).getStatus() >= curStatus) {
                couldAdd.add(sortedGuests.get(i));
                sumStatus = sumStatus + sortedGuests.get(i).getStatus();
            }
        }
        int curTotal = 0;
        for (int i = 0; i < couldAdd.size(); i++) {
            statusProbs.add((float) ((couldAdd.get(i).getStatus() + curTotal) / sumStatus));
            curTotal = curTotal + couldAdd.get(i).getStatus();
        }
        double rand = Math.random();
        for (int i = 0; i < couldAdd.size(); i++) {
            if (rand <= statusProbs.get(i)) {
                this.justAdded.add(couldAdd.get(i));
                this.guestList.add(couldAdd.get(i));
                couldAdd.get(i).insert();
                break;
            }
        }
    }

    public ArrayList<Guest> sortGuests() {
        ArrayList<Guest> sorted = new ArrayList<Guest>();
        for (int i = 0; i < this.allPossibleGuests.size(); i++) {
            if (!this.guestList.contains(this.allPossibleGuests.get(i))) {
                sorted.add(this.allPossibleGuests.get(i));
            }
        }
        return sorted;
    }
}
