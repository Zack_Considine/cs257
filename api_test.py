'''
    api_test.py
    Kirsten Baker and Zack Considine, 15 April 2016

    A program to retrieve results from the Pokemon API
    at http://pokeapi.co/ for Software Design, Spring 2016.
'''
#Import sys, argparse, json and urllib.request in order to
#handle data taken and requested from the server
import sys
import argparse
import json
import urllib.request

# Method for calling the server, and getting the
# list of dictionaries for a specific pokemon number
# It returns a list with the pokemon's name at the front
# as well as the list of the info straight from the server
def get_pokemon_info(pokemon, number):
    # format the url with the variables passed in
	base_url = 'http://pokeapi.co/api/v2/{0}/{1}/'
	url = base_url.format(pokemon, number)

    # try to connect to the given url
    # if this is not possible, simply return empty lists
	try:
        # The headers are necessary to bypass security features
        # for the pokeapi not allowing python to access its servers
		req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
		data_from_server = urllib.request.urlopen(req).read()
		string_from_server = data_from_server.decode('utf-8')
		pokemon_info_list = json.loads(string_from_server)

	except Exception as e:
        # Problems with network access or JSON parsing.
		return [], []

    # Initialize the result_list
	result_list = []

    # Try to parse through the info to take the name of
    # the pokemon. If failed just move along
	try:
		result_list.append(pokemon_info_list.get("name"))
		if type(pokemon_info_list.get("name")) != type(''):
			raise Exception('name has wrong type: "{0}"'.format(pokemon_info_list.get("name")))
	except Exception as e:
		pass

	return result_list, pokemon_info_list

# Method for calling the server, and getting the
# list of dictionaries for a specific type number
# It returns a list with the type at the front
# as well as the list of the info straight from the server
# This method is separate from the get_pokemon_info because
# it is accessed with different arguments in the main.
def get_type_info(types, type_entry):
    # format the url with the variables passed in
	base_url = 'http://pokeapi.co/api/v2/{0}/{1}/'
	url = base_url.format(types, type_entry)

    # try to connect to the given url
    # if this is not possible, simply return empty lists
	try:
		req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
		data_from_server = urllib.request.urlopen(req).read()
		string_from_server = data_from_server.decode('utf-8')
		type_info_list = json.loads(string_from_server)

	except Exception as e:
        # Problems with network access or JSON parsing.
		return []
	result_list = []

    # Try to parse through the info to take the name of
    # the pokemon. If failed just move along
	try:
		result_list.append(type_info_list.get("name"))
		if type(type_info_list.get("name")) != type(''):
			raise Exception('name has wrong type: "{0}"'.format(type_info_list.get("name")))
	except Exception as e:
		pass

	return result_list, type_info_list

# Method for parsing through the pokemon_info_list
# and retrieving the type or types of the pokemon
# Returns a result list with the name of the pokemon
# first and the types next
def get_pokemon_type(pokemon, number):
    #call the get_pokemon_info method to get the info needed
	result_list, pokemon_info_list = get_pokemon_info(pokemon, number)

    # The info list is a dictionary now so calling .get('types')
    # gives us a list of the type dictionaries
	type_list = pokemon_info_list.get('types')

    # Loop through this new list to retrieve each type
    # from their respective dictionaries
	for i in range(len(type_list)):
		try:
            # Each type has a name as well as a url link
            # to its own info library. We need just the name.
			type_name = type_list[i].get("type")
			actual_type = type_name.get("name")
			if type(actual_type) != type(''):
				raise Exception('pokemon type has wrong type: "{0}"'.format(actual_type))
			result_list.append(actual_type)
		except Exception as e:
			pass

	return result_list

# This method is very similar to get_pokemon_type
# The difference is in what we are parsing for
def get_pokemon_abilities(pokemon, number):
	result_list, pokemon_info_list = get_pokemon_info(pokemon, number)

    # Like with type, we need to get the list of dictionaries
    # associated with 'abilities' now.
	ability_list = pokemon_info_list.get('abilities')
	for i in range(len(ability_list)):
		try:
			ability_name = ability_list[i].get("ability")
			ability = ability_name.get("name")
			if type(ability) != type(''):
				raise Exception('pokemon ability has wrong type: "{0}"'.format(ability))
			result_list.append(ability)
		except Exception as e:
			pass

	return result_list

# This method is very similar to get_pokemon_type
# The difference is in what we are parsing for
def get_pokemon_height(pokemon, number):
	result_list, pokemon_info_list = get_pokemon_info(pokemon, number)

    # This info list only has one keyword associated with the
    # pokemon's height so we do not need to parse through the list
    # like we did in previous methods
	try:
        # The height is stored as an integer with the last digit
        # as a tenth of a meter. Therefore we convert to a float
        # first and then convert it to a string to append to our list
		height = pokemon_info_list.get("height")/10
		if type(str(height)) != type(''):
			raise Exception('pokemon height has wrong type: "{0}"'.format(height))
		result_list.append(str(height))
	except Exception as e:
		pass

	return result_list

# This method works very similarly to get_pokemon_height
def get_pokemon_weight(pokemon, number):
	result_list, pokemon_info_list = get_pokemon_info(pokemon, number)

	try:
		weight = pokemon_info_list.get("weight")/10
		if type(str(weight)) != type(''):
			raise Exception('pokemon weight has wrong type: "{0}"'.format(weight))
		result_list.append(str(weight))
	except Exception as e:
		pass

	return result_list

# This method works very similarly to get_pokemon_height
def get_pokemon_name(pokemon, number):
	result_list, pokemon_info_list = get_pokemon_info(pokemon, number)

	try:
		name = pokemon_info_list.get("name")
		if type(name) != type(''):
			raise Exception('pokemon name has wrong type: "{0}"'.format(name))
		result_list.append(name)
	except Exception as e:
		pass
	return result_list

# This method is not like the others because it has a tighter restriction
# on the number that can be passed as the second parameter as well
# as the fact that it calls get_type_info instead of get_pokemon_info
# It still returns a result_list with the name of the type at the
# front followed by all of the pokemon with that type
def get_type_list(types, type_entry):
    # Since there are only 18 types in the pokemon world
    # check to see that the number was correctly passed in.
	if int(type_entry) > 18:
		raise Exception('type number is too high: "{0}"'.format(type_entry))

    # Call get_type_info for the result list and the info list
	result_list, type_info_list = get_type_info(types, type_entry)

    # Accessing this dictionary using the keyword 'pokemon' gives
    # us a list of the dictionaries of pokemon names
	pokemon_list = type_info_list.get('pokemon')

    # Loop through these dictionaries and append all of the names
    # to the result list
	for i in range(len(pokemon_list)):
		try:
			pokemon_name = pokemon_list[i].get("pokemon")
			pokemon = pokemon_name.get("name")
			if type(pokemon) != type(''):
				raise Exception('pokemon has wrong type: "{0}"'.format(pokemon))
			result_list.append(pokemon)
		except Exception as e:
			pass

	return result_list

# Main method which checks the arguments passed and determines which
# methods to call and thus which output to print
def main(args):
    # The outline for finding a pokemon's type(s)
    if args.action == 'types':
        types = get_pokemon_type(args.label, args.number)
        if len(types) > 2:
        	print('{0}\'s types: {1} and {2}'.format(types[0], types[1], types[2]))
        else:
        	print('{0}\'s type: {1}'.format(types[0], types[1]))

    # The outline for finding a pokemon's abilities
    elif args.action == 'abilities':
    	abilities = get_pokemon_abilities(args.label, args.number)
    	if len(abilities) == 4:
    		print('{0}\'s abilities: {1}, {2}, and {3}'.format(abilities[0], abilities[1],
    			abilities[2], abilities[3]))
    	elif len(abilities) == 3:
    		print('{0}\'s abilities: {1} and {2}'.format(abilities[0], abilities[1], abilities[2]))
    	elif len(abilities) == 2:
    		print('{0}\'s ability: {1}'.format(abilities[0], abilities[1]))

    # The outline for finding a pokemon's height
    if args.action == 'height':
        height = get_pokemon_height(args.label, args.number)
        print('{0}\'s height: {1} meters'.format(height[0], height[1]))

    # The outline for finding a pokemon's weight
    if args.action == 'weight':
        weight = get_pokemon_weight(args.label, args.number)
        print('{0}\'s weight: {1} kilograms'.format(weight[0], weight[1]))

    # The outline for finding all pokemon with a given type
    if args.action == 'pokemon_by_type':
    	type_list = get_type_list(args.label, args.number)
    	num = len(type_list) - 1
    	print('List of the {1} pokemon with the {0} type:'.format(type_list[0], str(num)))
    	for i in range(len(type_list) - 2):
    		print(type_list[i+1], end=", ")
    	print("and " + type_list[-1])

    # The outline for finding a pokemon's name (number always given)
    if args.action == 'name':
    	name = get_pokemon_name(args.label, args.number)
    	print('Pokemon number ' + str(args.number) + ' is ' + name[0] + '.')

# Arg parsing that is run first...
if __name__ == '__main__':
    # Initialize a list of choices for the 'number' Argument
    # It can 1-721 because that is the number of total pokemon
	choices_list=[]
	nums = 721
	for i in range(nums):
		choices_list.append(str(i+1))

	parser = argparse.ArgumentParser(description='Get Pokemon info!')

    # The valid inputs for the first argument 'action'
    # which determines which function to call
	parser.add_argument('action',
						metavar='action',
						help='give an action to call a function',
						choices=['types', 'weight', 'height', 'abilities', 'pokemon_by_type', 'name'])
    # The valid inputs for the second argument 'label'
    # which determines the first directory in the url to be requested
    # either 'pokemon' or 'type'
	parser.add_argument('label',
						metavar='label',
						help='the pokemon as a pokemon',
						choices=['pokemon','type'])
    # The valid inputs for the last argument 'number'
    # which determines which pokemon to access or
    # which type to access
	parser.add_argument('number',
	                    metavar='number',
	                    help='gives Pokemon\'s pokedex number',
	                    choices=choices_list)

	args = parser.parse_args()
	main(args)
