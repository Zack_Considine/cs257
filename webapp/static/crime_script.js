/*Javascript for Crimes R Us webapp
 * 
* Zack Considine and Kirsten Baker
* 
* */

function loadData() {

    // get dropdown information on button click
    var crimeSelect= document.getElementById("crimes"); 
    var strCrime = crimeSelect.options[crimeSelect.selectedIndex].value;
    var yearSelect= document.getElementById("year");
    var strYear = yearSelect.options[yearSelect.selectedIndex].value;
    var regionSelect= document.getElementById("region");
    var strRegion = regionSelect.options[regionSelect.selectedIndex].value;
    var grab_url = "/crimes/"+strCrime+"/"+strRegion+"/"+strYear;

    if (strYear == "all_years" && strRegion == "all_regions") {
        alert("Please select either a specific year or a specific region. If you'd like to know about both, please go to https://www.fbi.gov/stats-services/crimestats to learn more! ");          
    }
    xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.open('get', grab_url);

    xmlHttpRequest.onreadystatechange = function() {
        
        if (xmlHttpRequest.readyState == 4 && xmlHttpRequest.status == 200) {
            getTheDeets(xmlHttpRequest.responseText, strRegion, strYear);
        }
    };

    xmlHttpRequest.send(null);
}

function getTheDeets(responseText, region, year) {
    // parse api data based on call and
    // go to appropriate display function
    var dataD = JSON.parse(responseText);
    var lenData = dataD.length;

    if (lenData == 1) {
        displaySingle(dataD);
        displaySingleLoc(dataD);
    }
    if (lenData == 4) {
        displayAllRegions(dataD, year);
    }
    if (lenData == 10) {
        displayAllYears(dataD, region);
    }
}

function displaySingle(dat) {
    // display some brief text about the single data point returned
    var percentD = dat[0];
    var percent = percentD['percent_pop'];    
    var cur_crime = document.getElementById("crimes");
    var crimeI = cur_crime.options[cur_crime.selectedIndex].value;
    var crime = crimeI.replace("_"," ");
    var cur_year = percentD['year'];
    var cur_region = percentD['location_id'];
    var regions = ['Northeast', 'Midwest', 'South', 'West'];
    var region = regions[cur_region - 1];

    var displayStr = "According to our data, "+percent.toString()+"% of the total amount of "+crime+" committed in the US during "+cur_year+" happened in the "+region+".";
    var stringDest = document.getElementById('results_str');
    stringDest.innerHTML = displayStr;
    document.getElementById("c").style.visibility = "hidden";
    
}

function displaySingleLoc(dat) {
    // also print the population of the given region based
    // on the single data point
    var percentD = dat[0];
    var percent = percentD['percent_pop'];
    var cur_year = percentD['year'];
    var cur_region = percentD['location_id'];
    var regions = ['Northeast', 'Midwest', 'South', 'West'];
    var region = regions[cur_region - 1];
    var locationStr = "The percent of the US population in "+region+" was "+percent.toString()+"% in "+cur_year+". ";
    var locationDest = document.getElementById('results_str2');
    locationDest.innerHTML = locationStr;
}
            
function displayAllRegions(dat, year) {
    //populate list of data for chart later
    var crimeData = [];
    for (var k = 0; k < 4; k++) {
        crimeData.push(dat[k]['percent_pop']);
    }
    getLocationDataForRegions(crimeData, year);
}

function getLocationDataForRegions(crimeData, year){
    // make a second api call to get location population data
    // for the given year
    location_url= "/locations/all_locations/"+(year.toString());
    xmlHttpRequestTwo = new XMLHttpRequest();
    xmlHttpRequestTwo.open('get', location_url);

    xmlHttpRequestTwo.onreadystatechange = function() {
        if (xmlHttpRequestTwo.readyState == 4 && xmlHttpRequestTwo.status == 200) {
            convertDataToArray(xmlHttpRequestTwo.responseText, crimeData);   
        }
    };

    xmlHttpRequestTwo.send(null);
}

function convertDataToArray(responseText, crimeData) {
    // function that converts both region and year JSON 
    // to chart appropriate data
    var locationData = JSON.parse(responseText);   
    var locationDataLen = locationData.length;
    responseArray = [];
    for (var k =0; k<locationDataLen; k++) {
        responseArray.push(locationData[k]['percent_pop']);
    }
    if (responseArray.length == 4) {        
        var regions = ['Northeast','Midwest', 'South', 'West'];
        createPieChart(regions, crimeData, responseArray);
    }
    if (responseArray.length == 10) { 
        var years = [2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014];
        createLineChart(years, crimeData, responseArray);
    }
}

function createLineChart(chartLabels, crimeData, locationData){
    // create line chart using chart.js and location data
    var ctx = document.getElementById("c").getContext("2d");
    var data = {
        labels: chartLabels, 
        datasets: [{
            label: "Percent of US that committed the given crime",
            fillColor: "#fff5cc",
            strokeColor: "#ffdb4d",
            pointColor: "#ffab00",
            data: crimeData
            }, 
            {label: "Percent of US that lives in the given region",
            fillColor: "rgba(151,0,205,0.5)",
            strokeColor : "rgba(151,0,205,1)",
            pointColor : "rgba(151,0,205,1)",
            data : locationData
        }]
    };
    var crimeChart = new Chart(ctx , {
        type: "line",
        data: data 
    });
    var chartEl = document.getElementById('results_chart');
    chartEl.innerHTML = crimeChart;
    document.getElementById("c").style.visibility = "visible";
    
    // clear information sidebar 
    var locationStr = "Look but don't touch! Mousing over this chart might make it disappear. (Sorry grader/Jeff) ";
    var locationDest = document.getElementById('results_str');
    locationDest.innerHTML = locationStr;

    var locationStr = " ";
    var locationDest = document.getElementById('results_str2');
    locationDest.innerHTML = locationStr;
}

function displayAllYears(dat, region) {
    // populate list of region data for chart 
    crimeData = [];
    for (var k = 0; k < 10; k++) {
        crimeData.push(dat[k]['percent_pop']);
    }
    getLocationDataForYears(crimeData, region);
}


function getLocationDataForYears(crimeData, region){
    // make api call for population data for the given region   
    location_url= "/locations/"+region+"/all_years/";
    xmlHttpRequestThree = new XMLHttpRequest();
    xmlHttpRequestThree.open('get', location_url);

    xmlHttpRequestThree.onreadystatechange = function() {
        if (xmlHttpRequestThree.readyState == 4 && xmlHttpRequestThree.status == 200) {
            convertDataToArray(xmlHttpRequestThree.responseText, crimeData);   
        }
    };

    xmlHttpRequestThree.send(null);
}


function createPieChart(chartLabels, crimeData, locationData){
    // make a pie chart using chart.js for the location data
    var ctx = document.getElementById("c").getContext("2d");
    var data = {
        labels: chartLabels, 
        datasets: [{
            label: "Percent of population who committed the given crime",
            backgroundColor: ["#FF6384","#36A2EB","#FFCE56"],
            hoverBackgroundColor: ["#FF6384","#36A2EB","#FFCE56"],
            data: crimeData
            }, 
            {label: "Percent of population in the given region",
            backgroundColor: ["#CC002C","#0e598b","#E6A400"],
            hoverBackgroundColor: ["#CC0031","#0e598f","#E6A408"],
            data : locationData
        }]
    };
    
    var crimeChart2 = new Chart(ctx , {
        type: "pie",
        data: data 
    });
    var chartEl = document.getElementById('results_chart');
    chartEl.innerHTML = crimeChart2;
    
    document.getElementById("c").style.visibility = "visible";
    
    // Add comments about the graph in the info sidebar
    var locationStr = "The outer ring shows the percent of the given crime committed by the given population, and the inner ring shows the percent of the population that lives in the region for the given year. ";
    var locationDest = document.getElementById('results_str');
    locationDest.innerHTML = locationStr;
    
    var locationStr = " ";
    var locationDest = document.getElementById('results_str2');
    locationDest.innerHTML = locationStr;
}

