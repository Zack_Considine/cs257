#!/usr/bin/env python3
'''
    crime_api.py
    Zack Considine and Kirsten Baker, 29 April 2016

    Simple Flask app used in the sample web app for
    CS 257, Spring 2016. This is the Flask app for the
    "crime" API and website. The API offers
    JSON access to the data.
    
    Original code written by Jeff Ondich. 
'''
import sys
import flask
import json
import config
import psycopg2

app = flask.Flask(__name__, static_folder='static', template_folder='templates')

def _fetch_all_rows_for_query(query):
    '''
    Returns a list of rows obtained from the books database
    by the specified SQL query. If the query fails for any reason,
    an empty list is returned.
    '''
    try:
        connection = psycopg2.connect(database=config.database, user=config.user, password=config.password)
    except Exception as e:
        print('Connection error:', e, file=sys.stderr)
        return []

    rows = []
    try:
        cursor = connection.cursor()
        cursor.execute(query)
        rows = cursor.fetchall() # This can be trouble if your query results are really big.
    except Exception as e:
        print('Error querying database:', e, file=sys.stderr)

    connection.close()
    
    return rows

@app.route('/')
def get_main_page():
    name = flask.request.args.get('name', 'Nameless One')
    return flask.render_template('index_homepage.html', name=name)

@app.route('/crimes/<crime>/<location_id>/<year>') 
def get_percent_crime_by_region_year(crime,location_id,year):
    '''
    Returns the percent of the population that committed the specified crime
    for the given region and year. 
    '''

    query = '''SELECT percent_pop FROM {0} 
               WHERE location_id = {1} 
               AND year = {2};'''.format(crime, location_id, year)
    
    crime_list = []
    for row in _fetch_all_rows_for_query(query):
        
        url = flask.url_for('get_percent_crime_by_region_year', crime=crime, location_id=location_id, year=year, _external=True)
        
        crime_info = {'location_id':location_id, 'year':year, 'percent_pop':row[0], 'url':url}
        crime_list.append(crime_info)

    return json.dumps(crime_list)

@app.route('/crimes/<crime>')
def get_crime_table(crime):
    '''
    Returns a list of all the stuff related to a specified crime.
    '''
    query = '''SELECT location_id, year, percent_pop
               FROM {0}
               ORDER BY location_id, year;'''.format(crime)

    crime_list = []
    for row in _fetch_all_rows_for_query(query):
        url = flask.url_for('get_crime_table', crime=row[0], _external=True)
        crime_info = {'location_id':row[0], 'year':row[1], 'percent_pop':row[2], 'url':url}
        crime_list.append(crime_info)

    return json.dumps(crime_list)


@app.route('/crimes/<crime>/<location_id>/all_years/') 
def get_percent_crime_by_region(crime,location_id):
    '''
    Returns the percent of the population that committed the specified crime
    for the given region for all years. 
    '''

    query = '''SELECT year, percent_pop FROM {0} 
               WHERE location_id = {1} 
               ORDER BY year;'''.format(crime, location_id)
    
    crime_list = []
    args = _fetch_all_rows_for_query(query)
    for row in args:
        
        url = flask.url_for('get_percent_crime_by_region', crime=crime, location_id=location_id, _external=True)
        
        crime_info = {'location_id':location_id, 'year':row[0], 'percent_pop':row[1], 'url':url}
        crime_list.append(crime_info)
    return json.dumps(crime_list)

@app.route('/crimes/<crime>/all_regions/<year>') 
def get_percent_crime_by_year(crime,year):
    '''
    Returns the percent of the population that committed the specified crime
    for the given year for all regions. 
    '''

    query = '''SELECT location_id, percent_pop FROM {0} 
               WHERE year = {1} 
               ORDER BY location_id;'''.format(crime, year)
    
    crime_list = []
    args = _fetch_all_rows_for_query(query)
    for row in args:
        url = flask.url_for('get_percent_crime_by_year', crime=crime, year=year, _external=True)
        crime_info = {'location_id':row[0], 'year':year, 'percent_pop':row[1], 'url':url}
        crime_list.append(crime_info)

    return json.dumps(crime_list)

@app.route('/locations/<location_id>/all_years/')
def get_percent_pop_by_region(location_id):
    '''
    Returns the percent of the population in a region compared to the total
    US population
    '''

    query = '''SELECT year, percent_pop FROM {0} 
               WHERE location_id = {1}
               ORDER BY year;'''.format('location', location_id)
    
    pop_list = []
    for row in _fetch_all_rows_for_query(query):
        
        url = flask.url_for('get_percent_pop_by_region', location_id=location_id, _external=True)
        
        pop_info = {'location_id':location_id, 'year':row[0], 'percent_pop':row[1], 'url':url}
        pop_list.append(pop_info)

    regions = ["Northeast", "Midwest", "South", "West"]
    region = regions[int(location_id) - 1]
    return json.dumps(pop_list)

@app.route('/locations/<location_id>/<year>') 
def get_percent_pop_by_region_year(location_id, year):
    '''
    Returns the percent of the population in a region compared to the total
    US population for a given year
    '''

    query = '''SELECT percent_pop FROM {0} 
               WHERE location_id = {1}
               AND year = {2};'''.format('location', location_id, year)
    
    pop_list = []
    for row in _fetch_all_rows_for_query(query):
        
        url = flask.url_for('get_percent_pop_by_region_year', location_id=location_id, year=year, _external=True)
        pop_info = {'location_id':location_id, 'year':year, 'percent_pop':row[0], 'url':url}
        pop_list.append(pop_info)

    regions = ["Northeast", "Midwest", "South", "West"]
    region = regions[int(location_id) - 1]
    return json.dumps(pop_list)


@app.route('/locations/all_locations/<year>') 
def get_percent_pop_by_year(year):
    '''
    Returns the percent of the population for all regions compared to the total
    US population in a given year
    '''

    query = '''SELECT location_id, percent_pop FROM {0} 
               WHERE year = {1} 
               ORDER BY location_id;'''.format('location', year)
    
    pop_list = []
    for row in _fetch_all_rows_for_query(query):
        
        url = flask.url_for('get_percent_pop_by_year', year=year, _external=True)
        pop_info = {'location_id':row[0], 'year':year, 'percent_pop':row[1], 'url':url}
        pop_list.append(pop_info)

    return json.dumps(pop_list)



if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: {0} host port'.format(sys.argv[0]), file=sys.stderr)
        exit()

    host = sys.argv[1]
    port = sys.argv[2]
    app.run(host=host, port=port)
    app.run(debug=True)


