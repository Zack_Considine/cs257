'''
    api_tests.py
    Kirsten Baker and Zack Considine, 15 April 2016

    A program to test our crime API for Software Design, Spring 2016.
'''

import json
import unittest
import urllib.request

def get_crime_info(crime, region, year):
	#TODO: change url once we know it
	base_url = 'http://crimesrus.com/crimes/{0}/{1}/{2}/'
	url = base_url.format(crime, region, year)

	try:
		req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
		data_from_server = urllib.request.urlopen(req).read()
		string_from_server = data_from_server.decode('utf-8')
		result_list = json.loads(string_from_server)

	except Exception as e:
        # Problems with network access or JSON parsing.
		return []
	
	return result_list

def get_location_info(region, year):
	#TODO: change url once we know it
	base_url = 'http://crimesrus.com/crimes/{0}/{1}/'
	url = base_url.format(crime, region, year)

	try:
		req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
		data_from_server = urllib.request.urlopen(req).read()
		string_from_server = data_from_server.decode('utf-8')
		result_list = json.loads(string_from_server)

	except Exception as e:
        # Problems with network access or JSON parsing.
		return []
	
	return result_list


class TestWebAppMethods(unittest.TestCase):

	def setUp(self):

	    self.database = web_api.createDatabase()


	def tearDown(self):

	    pass


	#crime and year
	def testTypicalCaseCrimeAndYear(self):

	    crime = "robbery"

	    year = 2014

	    result_expected = [{'region id':1, 'region name':'Northeast', 'Percent of Population': 18.0},
							{'region id':2, 'region name':'Midwest', 'Percent of Population': 18.8},
							{'region id':3, 'region name':'South', 'Percent of Population': 39.5},
							{'region id':4, 'region name':'West', 'Percent of Population': 23.7}];

	    result_real = get_crime_info(crime, 'all_regions', year)

	    assertEqual(result, expected)

	def testEmptyCaseCrimeAndYear(self):

	    crime = ""

	    year = 2014

	    assertRaises(URLError, get_crime_info(crime, 'all_regions', year))


	def testInvalidCaseCrimeAndYear(self):

	    crime = "robbery"

	    year = 2023

	    assertRaises(URLError, get_crime_info(crime, 'all_regions', year))

 	#crime and region
	def testTypicalCaseCrimeAndRegion(self):
 
		crime = "robbery"
 
		region = "northeast"

		result_expected = [{'year':2014, 'Percent of Population': 18.0},
							{'year':2013, 'Percent of Population': 19.1},
							{'year':2012, 'Percent of Population': 19.1},
							{'year':2011, 'Percent of Population': 19.4},
							{'year':2010, 'Percent of Population': 18.7},
							{'year':2009, 'Percent of Population': 17.3},
							{'year':2008, 'Percent of Population': 17.2},
							{'year':2007, 'Percent of Population': 16.9},
							{'year':2006, 'Percent of Population': 18.5},
							{'year':2005, 'Percent of Population': 19.4}];

		result_real = get_crime_info(crime, region, 'all_years')

		assertEqual(result, expected)
 
 
	def testEmptyCaseCrimeAndRegion(self):

		crime = ""

		region = "northeast"

		assertRaises(URLError, get_crime_info(crime, region, 'all_years'))

 
	def testInvalidCaseCrimeAndRegion(self):

		crime = "robbery"

		region = "Nowhere"

		assertRaises(URLError, get_crime_info(crime, region, 'all_years'))

	#crime, region, and year
	def testTypicalCaseCrimeRegionAndYear(self):

		crime = "robbery"

		region = "northeast"

		year = 2014

		result_expected = [{'Percent of Population': 18.0}];

		result_real = get_crime_info(crime, region, year)

		assertEqual(result, expected)
 
 
	def testEmptyCaseCrimeRegionAndYear(self):

		crime = "robbery"

		region = ""

		year = 2014

		assertRaises(URLError, get_crime_info(crime, region, year))
 
 
	def testInvalidCaseCrimeRegionAndYear(self):

		crime = "tattle-telling"

		region = "northeast"

		year = 2014

		assertRaises(URLError, get_crime_info(crime, region, year))

	#location and year
	def testTypicalCaseLocationAndYear(self):

		region = "northeast"

		year = 2014

		result_expected = [{'Percent of Population': 17.6}];

		result_real = get_location_info(region, year)

		assertEqual(result, expected)
 
 
	def testEmptyCaseLocationAndYear(self):

		region = ""

		year = 2014

		assertRaises(URLError, get_location_info(region, year))
 
 
	def testInvalidCaseLocationAndYear(self):

		region = "northsouth"

		year = 2014

		assertRaises(URLError, get_location_info(region, year))

	#location by year
	def testTypicalCaseLocationByYear(self):

		region = "northeast"

		result_expected = [{'year':2014, 'Percent of Population': 17.6},
							{'year':2013, 'Percent of Population': 17.7},
							{'year':2012, 'Percent of Population': 17.8},
							{'year':2011, 'Percent of Population': 17.8},
							{'year':2010, 'Percent of Population': 17.9},
							{'year':2009, 'Percent of Population': 18.0},
							{'year':2008, 'Percent of Population': 18.1},
							{'year':2007, 'Percent of Population': 18.1},
							{'year':2006, 'Percent of Population': 18.3},
							{'year':2005, 'Percent of Population': 18.4}];

		result_real = get_location_info(region, 'all_years')

		assertEqual(result, expected)
 
 
	def testEmptyCaseLocationByYear(self):

		region = ""

		assertRaises(URLError, get_location_info(region, 'all_years'))
 
 
	def testInvalidlCaseLocationByYear(self):

		region = "eastwest"

		assertRaises(URLError, get_location_info(region, 'all_years'))

	#all locations for year
	def testTypicalCaseAllLocationsAndYear(self):

		year = 2014

		result_expected = [{'region id':1, 'region name':'Northeast', 'Percent of Population': 17.6},
							{'region id':2,'region name':'Midwest', 'Percent of Population': 21.2},
							{'region id':3,'region name':'South', 'Percent of Population': 37.6},
							{'region id':4,'region name':'West', 'Percent of Population': 23.6}];

		result_real = get_location_info('all_regions', year)

		assertEqual(result, expected)
 
 
	def testEmptyCaseAllLocationsAndYear(self):

		year = ""

		assertRaises(URLError, get_location_info('all_regions', year))

 
	def testInvalidCaseAllLocationsAndYear(self):

		year = 201223

		assertRaises(URLError, get_location_info('all_regions', year))


